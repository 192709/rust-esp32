#!/bin/bash

print_usage() {
    echo "Usage: docker run [DEVICE OPTIONS] -v PATH_TO_RUST_CODE:/code DOCKER_IMAGE [OPTION]"
    echo "This Docker image supports cross compiling a Rust project at PATH_TO_RUST_CODE for an"
    echo "ESP32 device. It supports building the code, converting to an image, and flashing it to"
    echo "a connected device, or running a custom command provided at runtime."
    echo "NOTE: if you want to flash the image file to a device you will need to pass this device"
    echo "through to the dockerfile when running the container at the [DEVICE OPTIONS] location"
    echo "e.g.: docker run --device /dev/ttyUSB0 -v PATH_TO_RUST_CODE:/code DOCKER_IMAGE -f"
    echo
    echo "-b    build the Rust project"
    echo "-c    run the provided command"
    echo "-d    use the provided device when flashing the project"
    echo "      if this is not provided it will default to /dev/ttyUSB0"
    echo "-f    flash the Rust project to an ESP32 device"
    echo "-h    display this help and exit"
    echo "-i    convert the Rust project's binary output to an image file"
    echo "-v    explain what is being done"
}

while getopts "bc:d:fhiv" arg
do
    case $arg in
        b)
            buildCode=1
            ;;
        c)
            command="${OPTARG}"
            ;;
        d)
            device="${OPTARG}"
            ;;
        f)
            buildCode=1
            convertToImage=1
            flashImage=1
            ;;
        h)
            print_usage
            exit 0
            ;;
        i)
            buildCode=1
            convertToImage=1
            ;;
        v)
            verbose=1
            ;;
        ?)
            print_usage
            exit 1
            ;;
    esac
done

verbose_echo() {
    if [ $verbose ]
    then
        echo "$*"
    fi
}

# sudo esptool not working - re-add rust to dialout group? requires re-logging in...

# if a command was provided to run, just run that
if [ -n "$command" ]
then
    verbose_echo "Command was provided, running that: $command"
    eval $command
# otherwise look at running default commands based on provided parameters
else
    verbose_echo "Parsing Cargo.toml to determine binary name"
    cargoTomlNames="$(cat Cargo.toml | grep name | cut -d ' ' -f 3)"
    binaryName="$(echo ${cargoTomlNames} | cut -d '"' -f 2)"
    verbose_echo "Binary name is ${binaryName}"
    binaryPath="target/xtensa-esp32-none-elf/release"

    if [ $buildCode ]
    then
        verbose_echo "Building code"
        cargo xbuild --features "xtensa-lx-rt/lx6,xtensa-lx/lx6,esp32-hal" --release
    fi

    if [ $convertToImage ]
    then
        verbose_echo "Converting output to image"
        esptool.py --chip esp32 elf2image "${binaryPath}/${binaryName}" > /dev/null
    fi

    if [ $flashImage ]
    then
        # use ttyUSB0 by default if no device is specified
        if [ -z "$device" ]
        then
            verbose_echo "No device provided, using default"
            device="/dev/ttyUSB0"
        fi

        verbose_echo "Flashing image to $device"
        sudo esptool.py -p $device -b 115200 write_flash 0x10000 "${binaryPath}/${binaryName}.bin"
    fi
fi
