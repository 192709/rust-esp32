FROM debian:stable-20201012 AS builder

# Install dependencies to build Rust
RUN apt update -y && apt upgrade -y && \
    apt install -y \
        cmake \
        curl \
        g++ \
        gcc \
        git \
        make \
        ninja-build \
        libssl-dev \
        python \
        pkg-config

ENV HOME /root

# Fetch MabezDev's fork of Rust
WORKDIR ${HOME}
RUN git clone https://github.com/MabezDev/rust-xtensa

# Build Rust with Xtensa support
WORKDIR ${HOME}/rust-xtensa
RUN ./configure --experimental-targets=Xtensa && \
    ./x.py build --stage 2

# Fetch ESP32 tooling
WORKDIR ${HOME}
ARG ESP_TOOLS_URL_BASE=https://github.com/espressif/crosstool-NG/releases/download
ARG ESP_TOOLS_VERSION=esp-2020r3
ARG ESP_TOOLS_FILE=xtensa-esp32-elf-gcc8_4_0-esp-2020r3-linux-amd64.tar.gz
ARG ESP_TOOLS_URL=${ESP_TOOLS_URL_BASE}/${ESP_TOOLS_VERSION}/${ESP_TOOLS_FILE}
RUN curl -L ${ESP_TOOLS_URL} --output esp_tools.tar.gz && \
    tar xf esp_tools.tar.gz

# Use a fresh image to drop the 10+ GiB of cruft from building Rust
FROM debian:stable-20201012

# Install gcc so Rust can compile projects
RUN apt update -y && apt upgrade -y && \
    apt install -y \
        curl \
        gcc \
        git \
        nano \
        openssl \
        python3 \
        python3-pip \
        sudo

# esptool allows us to convert the ELF binary into an image and flash it to an ESP32
RUN pip3 install esptool

# Create a `rust` user that will be used for building code, so projects built
# with this image will be owned by user 1000, rather than root
ARG RUST_USER=rust
# Set RUST_USER to an environment variable so it is available in startup.sh
ENV RUST_USER ${RUST_USER}
ARG RUST_USER_PASSWORD=rust_builder
ENV RUST_HOME /home/${RUST_USER}
RUN useradd -d ${RUST_HOME} -m -p ${RUST_USER_PASSWORD} ${RUST_USER} && \
    usermod -aG sudo ${RUST_USER} && \
    sed -i 's/%sudo\tALL=(ALL:ALL) ALL/%sudo\tALL=(ALL) NOPASSWD: ALL/' /etc/sudoers

# Copy output from building Rust into the fresh image
COPY --chown=${RUST_USER}:${RUST_USER} --from=builder \
    /root/rust-xtensa/Cargo.* ${RUST_HOME}/rust-xtensa/
COPY --chown=${RUST_USER}:${RUST_USER} --from=builder \
    /root/rust-xtensa/library ${RUST_HOME}/rust-xtensa/library
COPY --chown=${RUST_USER}:${RUST_USER} --from=builder \
    /root/rust-xtensa/build/x86_64-unknown-linux-gnu/stage2 ${RUST_HOME}/rust-xtensa/build/x86_64-unknown-linux-gnu/stage2

# Copy ESP32 tooling into the fresh image
COPY --chown=${RUST_USER}:${RUST_USER} --from=builder \
    /root/xtensa-esp32-elf ${RUST_HOME}/xtensa-esp32-elf

# Setup /code directory for using to build projects
RUN mkdir /code && chown ${RUST_USER}:${RUST_USER} /code

# Switch to new user to install rustup, Rust, cargo-xbuild, cargo-espflash, and esptool
USER ${RUST_USER}

# Grab rustup and use it to install rust nightly, cargo-xbuild, and cargo-espflash
WORKDIR ${RUST_HOME}
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rustup.sh && \
    sh rustup.sh --default-toolchain nightly --profile default -y && \
    ${RUST_HOME}/.cargo/bin/cargo install cargo-xbuild cargo-espflash && \
    rm -rf rustup.sh

# switch back to root to copy and chmod startup.sh script
USER root

# Copy startup.sh and build.sh into the image, and run startup.sh by default.
# startup.sh will check arguments to determine if we should flash the output to a device,
# if so, startup.sh will handle checking for the /dev/ttyUSB0 device and if no device is
# found it will halt, otherwise it will handle allowing RUST_USER to access it.
# startup.sh then calls build.sh as RUST_USER, passing all provided arguments to it
# build.sh will check arguments to determine if we should:
#   * just compile the code into an ELF file
#   * convert the compiled ELF file into an ESP32 image
#   * flash the ESP32 image to /dev/ttyUSB0
# then run the appropriate commands
COPY startup.sh /
RUN chmod +x /startup.sh
ENTRYPOINT [ "/startup.sh" ]

# switch back to RUST_USER to run the container
USER ${RUST_USER}

# Configure environmental variables pointing to the Xtensa Rust binaries
ENV RUSTC ${RUST_HOME}/rust-xtensa/build/x86_64-unknown-linux-gnu/stage2/bin/rustc
ENV RUSTDOC ${RUST_HOME}/rust-xtensa/build/x86_64-unknown-linux-gnu/stage2/bin/rustdoc
ENV XARGO_RUST_SRC ${RUST_HOME}/rust-xtensa/library
ENV ESP32_TOOLING ${RUST_HOME}/xtensa-esp32-elf/bin
ENV PATH "${PATH}:${RUST_HOME}/.cargo/bin:${ESP32_TOOLING}"

# Ensure we are able to build a test project
WORKDIR ${RUST_HOME}
RUN git clone https://github.com/MabezDev/xtensa-rust-quickstart.git && \
    cd xtensa-rust-quickstart && \
    cargo xbuild --features "xtensa-lx-rt/lx6,xtensa-lx/lx6,esp32-hal" --release && \
    cd .. && \
    rm -rf xtensa-rust-quickstart

WORKDIR /code
